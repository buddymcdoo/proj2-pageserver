from flask import Flask, render_template

app = Flask(__name__)

@app.route("/")
def home():
    return render_template('index.html')

@app.route("/<path:name>")
def loadForm(name):
    try:
        if(name[0] == "~" or name[0] == "/" or (name[0] == "." and name[1] == ".")):
            return page_forbidden(name)
        else:
            return render_template(name), 200
    except:
        return page_not_found(name)


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

@app.errorhandler(403)
def page_forbidden(e):
    return render_template('403.html'), 403

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
