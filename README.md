#proj2-pageserver
#------------------
This is a simple server program created to practice using Docker and Flask.
It atempts to load a webpage, by checking if the page exists, and throwing
appropriate errors accordingly.

Author: Simon Ward
Contact Address: simonward3000@gmail.com
